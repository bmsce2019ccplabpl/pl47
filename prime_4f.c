#include<stdio.h>
int input();
int compute(int);
void output(int,int);

int main()
{
    int num,count;
    num=input();
    count=compute(num);
    output(count,num);
    return 0;
}

int input()
{
    int n;
    printf("Enter the number\n");
    scanf("%d",&n);
    return n;
}

int compute(int n)
{
    int c=0;
    for(int i=2;i<n;i++)
    {
        if(n%i==0)
            c++;
    }
    return c;
}
 void output(int c,int n)
{
    if(c==0)
        printf("The number %d is a prime number\n",n);
    else
        printf("The number %d is a composite number\n",n);
}