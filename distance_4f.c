#include<stdio.h>
#include<math.h>
float input(char,int);
float compute(float,float,float,float);
void output(float,float,float,float,float);

int main()
{
    float x1,y1,x2,y2,dist;
    x1=input('x',1);
    y1=input('y',1);
    x2=input('x',2);
    y2=input('y',2);
    dist=compute(x1,y1,x2,y2);
    output(x1,y1,x2,y2,dist);
    return 0;
}

float input(char g,int x)
{
    float p;
    printf("Enter the value of %c%d\n",g,x);
    scanf("%f",&p);
    return p;
}

float compute(float x1,float y1,float x2,float y2)
{
    float d;
    d=sqrt(pow((x2-x1),2)+pow((y2-y1),2));
    return d;
}

void output(float x1,float y1,float x2,float y2,float d)
{
    printf("The distance between (%f,%f) and (%f,%f) is %f units\n",x1,y1,x2,y2,d);
}