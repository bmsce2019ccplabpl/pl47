#include <stdio.h>
int input();
int compute(int);
void output(int,int);

int main()
{   
    int n,rev;
    n=input();
    rev=compute(n);
    output(rev,n);
}

int input()
{
    int a;
    printf("Enter the number\n");
    scanf("%d",&a);
    return a;
}

int compute(int n)
{
    int rem,rev=0,temp=n;
    while(temp>0)
    {
        rem=temp%10;
        rev=rev*10+rem;
        temp=temp/10;
    }
    return rev;
}

void output(int rev, int n)
{
    printf("The reversed number is%d\n",rev);
    if(rev==n)
        printf("The given number %d is a palindrome number\n",n);
    else
        printf("The given number %d is not a palindrome number\n",n);
}