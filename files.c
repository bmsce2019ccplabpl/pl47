#include<stdio.h>

int main()
{
    char a;
    FILE *fp;
    printf("Enter the contents of the file:\n");
    fp=fopen("input.txt","w");
    while((a=getchar())!=EOF)
    {
        fputc(a,fp);
    }
    fclose(fp);
    printf("\nThe contents of the file is:\n");
    fp=fopen("input.txt","r");
    while((a=fgetc(fp))!=EOF)
        printf("%c",a);
    fclose(fp);
    return 0;
}