#include <stdio.h>
int input();
int vote(int);
void output(int);

int main()
{
    int age,x;
    age=input();
    x=vote(age);
    output(x);
    return 0;
}

int input()
{
    int a;
    printf("Enter the age\n");
    scanf("%d",&a);
    return a;
}

int vote(int a)
{
    if(a>=18)
        return 1;
    else
        return 0;
}

void output(int y)
{
    if(y==1)
        printf("You are eligible to vote");
    else
        printf("You are ineligible to vote");
}
