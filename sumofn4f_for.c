#include <stdio.h>
int input();
int compute(int);
void output(int,int);

int main()
{
	int sum,num;
    num=input();
    sum=compute(num);
    output(num,sum);
}

int input()
{
    int a;
    printf("Enter the number\n");
    scanf("%d",&a);
    return a;
}

int compute(int n1)
{
    int s=0,r;
    for(;n1>0;(n1=n1/10))
    {
        r=n1%10;
        s=s+r;
    }
    return s;
}

void output(int x, int y)
{
    printf("The sum of %d digits is %d\n",x,y);
}
