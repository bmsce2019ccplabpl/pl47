# include <stdio.h>
 
int main()
{
   int a, b, c, large ;
   printf("Enter three numbers : \n") ;
   scanf("%d %d %d", &a, &b, &c) ;
   large = a > b ? (a > c ? a : c) : (b > c ? b : c) ;
   printf("\nThe biggest number is : %d\n", large) ;
   return 0;
}
