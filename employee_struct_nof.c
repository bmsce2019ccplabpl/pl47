#include<stdio.h>

int main()
{
    struct employee
    {
        char name[20], dept[20], doj[10];
        int eid, salary;
    };
    struct employee E;
    printf("Enter the Employee Name, Employee ID, Department, Date Of Joining and Salary\n");
    scanf("%s%d%s%s%d",E.name,&E.eid,E.dept,E.doj,&E.salary);
    printf("THE EMPLOYEE DETAILS ARE AS FOLLOWS:\nEmployee Name:%s\nEmployee ID:%d\nDepartment:%s\nDate Of Joining:%s\nSalary:%d\n",E.name,E.eid,E.dept,E.doj,E.salary);
    return 0;
}