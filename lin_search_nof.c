#include<stdio.h>

int main()
{
    int n,pos,ele,k=0;
    printf("Enter the number of elements in the array\n");
    scanf("%d",&n);
    int a[n];
    printf("Enter the array elements\n");
    for(int i=0;i<n;i++)
    scanf("%d",&a[i]);
    printf("Enter the element to be searched\n");
    scanf("%d",&ele);
    for(int j=0;j<n;j++)
    {
        if(a[j]==ele)
        {
            k=1;
            pos=j;
            printf("The element %d is found in the index %d\n",ele,pos);
            break;
        }
    }
    if(k==0)
        printf("The element %d is not present in the array\n",ele);
    return 0;
}