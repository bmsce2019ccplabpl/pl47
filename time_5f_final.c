#include <stdio.h>
int input1();
int input2();
int compute(int,int);
void output(int);
int main()
{
    int hours,min,total;
    hours=input1();
    min=input2();
    total=compute(hours,min);
    output(total);
    return 0;
}

int input1()
{
    printf("Enter the hours\n");
    scanf("%d",&hours);
    return hours;
}

int input2()
{
    printf("Enter the minutes\n");
    scanf("%d",&min);
    return min;
}

int compute(int hours,int min)
{
    int a,res;
    a=hours*60;
    res=a+min;
    return res;
}

void output(int c)
{
    printf("The total minutes=%d",c);
}