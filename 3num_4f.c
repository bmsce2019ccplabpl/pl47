#include <stdio.h>
int input(int);
int comp(int,int,int);
void output(int,int,int,int);

int main()
{
    int a,b,c,large;
    a=input(1);
    b=input(2);
    c=input(3);        
    large=comp(a,b,c);
    output(large,a,b,c);
    return 0;
}

int input(int g)
{
    int n;
    printf("Enter the value of number %d\n",g);
    scanf("%d", &n);
    return n;
}

int comp(int x,int y,int z)
{
    int l;
    if(x>=y)
    {
        if(x>=z)
            l=x;
        else 
            l=z;
    }
    else if(y>=z)
        l=y;
    else 
        l=z;
    return l;
}

void output(int l1, int x1, int y1, int z1)
{
    printf("The largest among %d, %d and %d is %d\n",x1,y1,z1,l1);
}