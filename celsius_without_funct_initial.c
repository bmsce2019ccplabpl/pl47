#include <stdio.h>

int main()
{
    float c, f;

    printf("Enter temperature in Fahrenheit: ");
    scanf("%f", &f);
    c=(f- 32) * 5 / 9;
    printf("%f Fahrenheit = %f Celsius", f, c);
    return 0;
}

