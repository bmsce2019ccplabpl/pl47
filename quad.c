#include <stdio.h>
#include <math.h> 

int main()
{
    float a, b, c;
    float root1, root2;
    float discriminant;
    printf("Enter values of a, b, c of the quadratic equation :\n ");
    scanf("%f%f%f", &a, &b, &c);
    discriminant = (b * b) - (4 * a * c);
    switch(discriminant > 0)
    {
        case 1:
            root1 = (-b + sqrt(discriminant)) / (2 * a);
            root2 = (-b - sqrt(discriminant)) / (2 * a);
            printf("Roots are real and distinct and the roots are: %f and %f\n", 
            root1, root2);
            break;
            
            case 0:
            switch(discriminant < 0)
            {
                case 1:
                    printf("Roots are imaginary\n");
                    break;
                    
                case 0:
                    root1 = root2 = -b / (2 * a);
                    printf("Roots are real and equal and the roots are %f and %f\n", root1, root2);
                    break;
            }
    }
    return 0;
}