#include<stdio.h>
int input(char s);
void swap(int *x,int *y);
void output(int g, int y);

int main()
{
    int a,b;
    a=input('a');
    b=input('b');
    swap(&a,&b);
    output(a,b);
} 

int input(char z)
{
    int x;
    printf("Enter the value of %c\n",z);
    scanf("%d",&x);
    return x;
}

void swap(int *j,int *k)
{
    int temp;
    temp=*j;
    *j=*k;
    *k=temp;
}

void output(int h,int i)
{
    printf("The value of a and b after swapping is %d and %d\n",h,i);
}