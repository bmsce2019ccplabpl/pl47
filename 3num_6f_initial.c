#include <stdio.h>
int input1();
int input2();
int input3();
int comp(int,int,int);
void output(int,int,int,int);

int main()
{
    int a,b,c,d;
    a=input1();
    b=input2();
    c=input3();        
    d=comp(a,b,c);
    output(d,a,b,c);
    return 0;
}

int input1()
{
    int a1;
    printf("Enter the first number\n");
    scanf("%d", &a1);
    return a1;
}

int input2()
{
    int b1;
    printf("Enter the second number\n");
    scanf("%d", &b1);
    return b1;
}

int input3()
{
    int c1;
    printf("Enter the third number\n");
    scanf("%d", &c1);
    return c1;
}

int comp(int x,int y,int z)
{
    if(x>=y)
    {
        if(x>=z)
            return 1;
        else 
            return 2;
    }
    if(y>=z)
        return 3;
    else 
        return 4;
}

void output(int d1, int x1, int y1, int z1)
{
    if(d1==1)
        printf("%d is the largest", x1);
    else if(d1==2)
        printf("%d is the largest", y1);
    else if(d1==3)
        printf("%d is the largest", y1);
    else 
         printf("%d is the largest", z1);
}