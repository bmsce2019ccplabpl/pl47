#include <stdio.h>
int input1();
int input2();
int large(int,int);
void output(int,int,int);
int main()
{
    int a,b,c;
    a=input1();
    b=input2();
    c=large(a,b);
    output(c,a,b);
    return 0;
}

int input1()
{
    int a1;
    printf("Enter the first number\n");
    scanf("%d",&a1);
    return a1;
}

int input2()
{
    int b1;
    printf("Enter the second number\n");
    scanf("%d",&b1);
    return b1;
}

int large(int x, int y)
{
    if(x>y)
        return 0;
    else if(y>x)
        return 1;
    else
        return 2;
}

void output(int z, int x1, int y1)
{
    if(z==0)
        printf("%d is the largest",x1);
    else if(z==1)
        printf("%d is the largest",y1);
    else
        printf("%d and %d are equal",x1,y1);
}