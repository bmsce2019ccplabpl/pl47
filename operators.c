#include <stdio.h>
int input(int);
int sum1(int,int);
int difference1(int,int);
int product1(int,int);
float quotient1(int,int);
int remainder1(int,int);
void output(int,int,int,int,int,float,int);

int main()
{

    int n1,n2,sum,difference,product,remainder;
    float quotient;
    n1=input(1);
    n2=input(2);
    sum=sum1(n1,n2);
    difference=difference1(n1,n2);
    product=product1(n1,n2);
    quotient=quotient1(n1,n2);
    remainder=remainder1(n1,n2);
    output(n1,n2,sum,difference,product,quotient,remainder);
    return 0;
}

int input(int g)
{
    int m;
    printf("Enter the value of number %d\n",g);
    scanf("%d",&m);
    return m;
}

int sum1(int x,int y)
{
    int s;
    s=x+y;
    return s;
}

int difference1(int x,int y)
{
    int d;
    d=x-y;
    return d;
}

int product1(int x,int y)
{
    int p;
    p=x*y;
    return p;
}

float quotient1(int x,int y)
{
    float q;
    q=(float)x/y;
    return q;
}

int remainder1(int x,int y)
{
    int r;
    r=x%y;
    return r;
}

void output(int x,int y,int s,int d,int p,float q,int r)
{
    printf("The following are the arithmetic operations carried on the two numbers %d and %d:\nSum=%d\nDifference=%d\nProduct=%d\nQuotient=%f\nRemainder=%d\n",x,y,s,d,p,q,r);
}