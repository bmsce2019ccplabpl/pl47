#include <stdio.h>
int input(int);
int large(int,int);
void output(int);
int main()
{
    int a,b,l;
    a=input(1);
    b=input(2);
    l=large(a,b);
    output(l);
    return 0;
}

int input(int g)
{
    int p;
    printf("Enter number %d\n",g);
    scanf("%d",&p);
    return p;
}

int large(int x, int y)
{
    int l;
    if(x>=y)
        l=x;
    else
        l=y;
    return l;
}

void output(int l1)
{
    printf("%d is the largest",l1);
}