#include <stdio.h>
int input();
int compute(int);
void output(int,int);
 
int main()
{
	int sum=0,i,n;
    n=input();
    sum=compute(n);
    output(n,sum);
}

int input()
{
    int a;
    printf("Enter the number\n");
    scanf("%d",&a);
    return a;
}

int compute(int n)
{
    int s=0, r;
    while(n>0)
    {
        r=n%10;
        s=s+r;
        n=n/10;
    }
    return s;
}

void output(int n, int sum)
{
    printf("The sum of %d digits is %d\n",n,sum);
}