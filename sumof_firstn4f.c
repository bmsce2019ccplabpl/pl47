#include <stdio.h>
int input();
int compute(int);
void output(int,int);

int main()
{   
    int n,sum;
    n=input();
    sum=compute(n);
    output(n,sum);
}

int input()
{
    int a;
    printf("Enter the number\n");
    scanf("%d",&a);
    return a;
}

int compute(int n)
{
    int i=n,sum=0;
    while(i>0)
    {
     sum=sum+i;
     i--;
    }
    return sum;
}

void output(int n, int sum)
{
    printf("The sum of first %d numbers is %d\n",n,sum);
}
