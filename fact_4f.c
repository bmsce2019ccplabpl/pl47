#include<stdio.h>
int input();
int compute(int);
void output(int,int);

int main()
{
    int num,fact;
    num=input();
    fact=compute(num);
    output(num,fact);
    return 0;
}

int input()
{
    int n;
    printf("Enter the number\n");
    scanf("%d",&n);
    return n;
}

int compute(int n)
{
    int f=1;
    for(int i=n;i>=1;i--)
    {
        f=f*i;
    }
    return f;
}

void output(int n,int f)
{
    printf("The factorial of %d is %d\n",n,f);
}
