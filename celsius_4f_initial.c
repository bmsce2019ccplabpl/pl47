#include <stdio.h>
float input();
float conv(float);
void output(float,float);

int main()
{
    float f,a;
    f=input(); 
    a=conv(f);
    output(f,a);
    return 0;
}

float input()
{
    float f1;
    printf("Enter temperature in Fahrenheit:\n");
    scanf("%f", &f1);
    return f1;
}

float conv(float x)
{
    float c;
    c=(x- 32) * 5 / 9;
    return c;
}

void output(float y,float z)
{
    printf("%f Fahrenheit = %f Celsius", y, z);
}